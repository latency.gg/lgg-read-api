import pg from "pg";
import { Promisable } from "type-fest";
import * as application from "../application/index.js";

export async function withPgTransaction<T>(
    context: application.Context,
    name: string,
    job: (pgClient: pg.ClientBase) => Promisable<T>,
): Promise<T> {
    let result: T;

    const pgClient = await context.services.pgPool.connect();
    try {
        const stopTimer = context.metrics.transactionDuration.startTimer({ name });
        await pgClient.query("BEGIN TRANSACTION;");
        try {
            result = await job(pgClient);

            await pgClient.query("COMMIT TRANSACTION;");
            stopTimer({ end: "commit" });
        }
        catch (error) {
            await pgClient.query("ROLLBACK TRANSACTION;");
            stopTimer({ end: "rollback" });

            throw error;
        }

        pgClient.release();
    }
    catch (error) {
        pgClient.release(true);

        throw error;
    }

    return result;
}
