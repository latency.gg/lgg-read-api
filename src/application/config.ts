import * as prom from "prom-client";

export interface Config {
    endpoint: URL;
    pgUri: URL;

    abortController: AbortController,
    promRegistry: prom.Registry;
}
